const express = require('express'),
    bodyParser = require('body-parser'),
    path = require('path'),
    app = express(),
    product = require('./api/product');


app.set('port', (process.env.PORT || 3000));

app.use(express.static(path.resolve(__dirname, '../public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', (req, res) => {
    res.sendFile('../public/index.html');
});

app.get('/api/k-market', (req, res) => {
    product
    .getKMarketProducts()
    .then((result) => {
        res.send(result);
    });
});

app.get('/api/prisma', (req, res) => {
    product
        .getPrismaProducts()
        .then((result) => {
            res.send(result);
        });
});


app.listen(app.get('port'), function() {
console.log('Server started on port ' + app.get('port'));
});