const axios = require('axios'),
    cheerio = require('cheerio');

module.exports = product = {

    getKMarketProducts() {

        (async () => {
            const products = await axios
                .get('https://www.k-market.fi/api/teaser/GetTeasers?blockId=248654&cacheBuster=0409201764320&page=0')
                .then((res) => {
                    res.data.teasers.forEach(function(item) {
                        item.market = 'K-Market';
                        item.showAdditionalInfo = false;
                    })
                    return res.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
            return products;
        })();
    },

    getPrismaProducts() {
        let products = [];

        (async () => {
            const result = await axios
                .get('https://www.foodie.fi/products/recommendations?main_view=1')
                .then((res) => {
                    const $ = cheerio.load(res.data);
                    const productsCount = $('div.name').length;

                    for (let i = 0; i < productsCount; i++) {
                        const product = {
                            title: $('div.name').eq(i).text(),
                            price: {
                                euros: $('span.whole-number').eq(i).text(),
                                cents: $('span.cents').eq(i).text(),
                                unit: $('unit').eq(i).text(),
                            },
                            market: 'Prisma',
                            showAdditionalInfo: false
                        }
                        products.push(product);
                    }
                    return products;
                })
                // .catch(function (error) {
                //     console.log(error);
                // });
            return result;
        })();
    }
}