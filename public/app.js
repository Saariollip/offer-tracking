new Vue({
    
    el: '#results',
    
    data: {
        items: []
    },
    
    created() {
      this.fetchData()
    },
    
    methods: {
      fetchData() {
        this.$http
          .get('./products')
          .then(result => {
             this.items = result.data;
             this.items.forEach(function(item) {
               item.showAdditionalInfo = false;
               item.market = 'K-Market';
              })
             });
        this.$http
          .get('./prisma')
          .then(result => {
            const shuffledArray = shuffleArray(this.items.concat(result.data));
            this.items = shuffledArray;
          })

      },
      toggle(item) {
        item.showAdditionalInfo = !item.showAdditionalInfo;
        // computed property somehow ?
        this.$forceUpdate();
      }
    },
})

function shuffleArray(array) {
  for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
  }
  return array;
}